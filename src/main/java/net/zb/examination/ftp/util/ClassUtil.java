package net.zb.examination.ftp.util;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@Slf4j
public class ClassUtil {

	private static final String FILE_TYPE = "file";

	/**
	 * 实例化指定包下的所有类对象
	 * @param packageName
	 * @return
	 */
	public static void initClass(String packageName) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		Enumeration<URL> urls = getClassLoader().getResources(packageName.replace(".", "/"));
		while (urls.hasMoreElements()) {
			URL url = urls.nextElement();
			String protocol = url.getProtocol();
			if (FILE_TYPE.equals(protocol)) {
				String basePath = url.getPath().replace("%20", "");
				initClass(basePath, packageName);
			}
		}
	}


	private static void initClass(String basePath, String packageName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		File[] files = new File(basePath).listFiles(file -> ((file.isFile() && file.getName().endsWith(".class")) || (file.isDirectory())));
		if(files == null || files.length == 0){
			return;
		}
		for (File file : files) {
			String fileName = file.getName();
			if (file.isFile()) {
				String className = fileName.substring(0, fileName.lastIndexOf("."));
				if (StrUtil.isNotEmpty(className)) {
					className = packageName + "." + className;
				}
				Class.forName(className).newInstance();
			} else {
				String subPath = fileName;
				if (StrUtil.isNotEmpty(subPath)) {
					subPath = packageName + "/" + subPath;
				}
				String subPackage = packageName;
				if (StrUtil.isNotEmpty(subPackage)) {
					subPackage = packageName + "." + fileName;
				}
				initClass(subPath, subPackage);
			}
		}
	}


	public static ClassLoader getClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}


	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		initClass("net.zb.examination.ftp.command");
	}

}
