package net.zb.examination.ftp.util;

import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.AbstractCommand;
import net.zb.examination.ftp.constant.CommandEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
public class CommandContainer {


	private static final Map<CommandEnum, AbstractCommand> commandMap = new ConcurrentHashMap<>();



	public static void add(CommandEnum commandEnum, AbstractCommand abstractCommand){
		commandMap.put(commandEnum, abstractCommand);
	}



	public static String handle(String code, ChannelHandlerContext ctx, String[] msg){
		CommandEnum commandEnum = CommandEnum.of(code);
		return commandMap.get(commandEnum).process(ctx, msg);
	}


}
