package net.zb.examination.ftp;

import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.constant.FtpCommand;
import net.zb.examination.ftp.util.CommandContainer;


/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
public abstract class AbstractCommand {


	protected AbstractCommand(){
		FtpCommand ftpCommand = this.getClass().getAnnotation(FtpCommand.class);
		if(ftpCommand == null){
			throw new IllegalArgumentException("加载类: [" + this.getClass() +"]失败，未配置FtpCommand注解");
		}
		CommandContainer.add(ftpCommand.value(), this);
	}


	public abstract String process(ChannelHandlerContext ctx, String[] msg);

}
