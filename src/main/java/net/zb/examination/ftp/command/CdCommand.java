package net.zb.examination.ftp.command;

import cn.hutool.core.util.StrUtil;
import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.AbstractCommand;
import net.zb.examination.ftp.UserContainer;
import net.zb.examination.ftp.constant.CommandEnum;
import net.zb.examination.ftp.constant.FtpCommand;
import net.zb.examination.ftp.constant.UserInfo;

import java.io.File;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/18, 初始化版本
 * @version 1.0
 **/
@FtpCommand(value = CommandEnum.CD)
public class CdCommand extends AbstractCommand {


	@Override
	public String process(ChannelHandlerContext ctx, String[] msg) {
		UserInfo userInfo = UserContainer.getUserInfo(ctx.channel().id());
		if(userInfo == null){
			ctx.close();
			return null;
		}
		if(msg.length != 2){
			return "未知的命令格式";
		}
		String newPath = StrUtil.addSuffixIfNot(userInfo.getDir(), File.separator) + msg[1];
		File file = new File(newPath);
		if(file.exists() && file.isDirectory()){
			userInfo.setDir(newPath);
			return "切换目录成功";
		}
		return "未知的路径";
	}


}
