package net.zb.examination.ftp.command;

import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.AbstractCommand;
import net.zb.examination.ftp.UserContainer;
import net.zb.examination.ftp.constant.CommandEnum;
import net.zb.examination.ftp.constant.FtpCommand;
import net.zb.examination.ftp.constant.UserInfo;

import java.io.File;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@FtpCommand(value = CommandEnum.DIR)
public class DirCommand extends AbstractCommand {

	@Override
	public String process(ChannelHandlerContext ctx, String[] msg) {
		UserInfo userInfo = UserContainer.getUserInfo(ctx.channel().id());
		if(userInfo == null){
			ctx.close();
			return null;
		}
		File dir = new File(userInfo.getDir());
		StringBuilder dirs = new StringBuilder();
		String[] dirList= dir.list();
		if(dirList == null){
			return "";
		}
		for(String name : dirList) {
			dirs.append("  ");
			dirs.append(name);

		}
		return dirs.toString();
	}

}
