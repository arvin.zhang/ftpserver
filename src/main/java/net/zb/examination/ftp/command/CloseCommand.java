package net.zb.examination.ftp.command;

import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.AbstractCommand;
import net.zb.examination.ftp.UserContainer;
import net.zb.examination.ftp.constant.CommandEnum;
import net.zb.examination.ftp.constant.FtpCommand;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/18, 初始化版本
 * @version 1.0
 **/
@FtpCommand(value = CommandEnum.CLOSE)
public class CloseCommand extends AbstractCommand {


	@Override
	public String process(ChannelHandlerContext ctx, String[] msg) {
		UserContainer.removeChannel(ctx);
		ctx.close();
		return null;
	}


}
