package net.zb.examination.ftp.command;

import io.netty.channel.ChannelHandlerContext;
import net.zb.examination.ftp.AbstractCommand;
import net.zb.examination.ftp.constant.CommandEnum;
import net.zb.examination.ftp.constant.FtpCommand;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@FtpCommand(value = CommandEnum.UN_KNOWN)
public class UnKnownCommand extends AbstractCommand {


	@Override
	public String process(ChannelHandlerContext ctx, String[] msg) {
		return null;
	}
}
