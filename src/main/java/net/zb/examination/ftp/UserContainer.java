package net.zb.examination.ftp;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import net.zb.examination.ftp.constant.UserInfo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/18, 初始化版本
 * @version 1.0
 **/
public class UserContainer {


	private static final Map<ChannelId, UserInfo> userMap = new ConcurrentHashMap<>();

	/**
	 * 新增channel
	 * @param ctx
	 */
	public static void addChannel(ChannelHandlerContext ctx){
		userMap.put(ctx.channel().id(), new UserInfo(ctx.channel()));
	}



	public static void removeChannel(ChannelHandlerContext ctx){
		userMap.remove(ctx.channel().id());
	}


	public static UserInfo getUserInfo(ChannelId channelId){
		return userMap.get(channelId);
	}

}
