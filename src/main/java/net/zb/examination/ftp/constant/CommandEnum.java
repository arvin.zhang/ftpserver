package net.zb.examination.ftp.constant;

import lombok.Getter;

import java.util.Objects;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@Getter
public enum  CommandEnum {

	DIR("LIST", "列出文件夹下文件"),
	CD("CWD", "到达指定目录"),
	CLOSE("CLOSE", "关闭连接"),
	GET("RETR", "获取文件"),
	EPRT("EPRT", "设置端口号"),
	UN_KNOWN("unKnown", "未知命令"),
	;

	CommandEnum(String code, String desc){
		this.code = code;
		this.desc = desc;
	}

	private String code;
	private String desc;



	public static CommandEnum of(String code){
		if(Objects.nonNull(code)){
			for (CommandEnum commandEnum : CommandEnum.class.getEnumConstants()){
				if(commandEnum.getCode().equalsIgnoreCase(code)){
					return commandEnum;
				}
			}
		}
		return UN_KNOWN;
	}

}
