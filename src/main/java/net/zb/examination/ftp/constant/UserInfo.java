package net.zb.examination.ftp.constant;

import cn.hutool.core.util.StrUtil;
import io.netty.channel.Channel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

	private Channel channel;
	private String dir;
	private String protocol;
	private String address;
	private Integer port;

	public UserInfo(Channel channel){
		this.channel = channel;
		this.dir = "D://";
	}


	public void initConnectionInfo(String msg){
		String[] connectionArray = StrUtil.split(msg, "|");
		this.protocol = connectionArray[1];
		this.address = connectionArray[2];
		this.port = Integer.parseInt(connectionArray[3]);

	}

}
