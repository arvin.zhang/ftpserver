package net.zb.examination.ftp.handlers;

import cn.hutool.core.util.StrUtil;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import net.zb.examination.ftp.UserContainer;
import net.zb.examination.ftp.util.CommandContainer;

import java.nio.charset.Charset;

/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
@Slf4j
public class FtpInBoundHandler extends SimpleChannelInboundHandler<String> {


	private static final Charset UTF8 = Charset.forName("UTF-8");
	private static final int CODE_CONNECT_SUCCESS = 220;


	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		String line = CODE_CONNECT_SUCCESS + " " + "FTP server (" + 1.0F + ") ready." + "\r\n";
		byte[] data = line.getBytes(UTF8);
		ctx.writeAndFlush(Unpooled.wrappedBuffer(data));
		UserContainer.addChannel(ctx);
	}


	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.info("链接断开: {}", ctx.channel().remoteAddress());

	}


	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
		log.info("收到命令: {}", msg);
		String[] cmdArray = StrUtil.split(msg, "");
		if(cmdArray.length < 1){
			return;
		}
		String ss = CommandContainer.handle(cmdArray[0], ctx, cmdArray);
		if(ss != null){
			ctx.writeAndFlush("200 " + ss + "\r\n");
		}else{
			ctx.writeAndFlush("200\r\n");
		}
	}

}
