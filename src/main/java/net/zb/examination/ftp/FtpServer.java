package net.zb.examination.ftp;

import net.zb.examination.ftp.util.ClassUtil;


/**
 * <p></p>
 *
 * @author bin.zhang
 * <p/>
 * Revision History:
 * 2020/04/15, 初始化版本
 * @version 1.0
 **/
public class FtpServer {


	private static final String COMMAND_PACKAGE = "net.zb.examination.ftp.command";

	public static void main(String[] args) throws Exception {
		ClassUtil.initClass(COMMAND_PACKAGE);
		NettyServerRunner nettyServerRunner = new NettyServerRunner(21);
		nettyServerRunner.run();
	}


}
